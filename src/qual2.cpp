/**
 * Copyright 2017 Davud Rusbarsky
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Approach
 * 
 * 
 */

#include <iostream>
#include <math.h>

#include <ros/ros.h>
#include <ros/subscribe_options.h>
#include <std_msgs/String.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/JointState.h>

#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>

// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>

#include <visualization_msgs/Marker.h>

#include <srcsim/Console.h>

#include <geometry_msgs/PointStamped.h>

#include <ihmc_msgs/FootstepDataListRosMessage.h>
#include <ihmc_msgs/FootstepDataRosMessage.h>
#include <ihmc_msgs/FootstepStatusRosMessage.h>

using namespace std;

// Note: Hue eventually gets stored in a uchar by OpenCV. Divide the normal hue by 2 to get the desired effect.
static const int RED_HUE           =   0; // h = h/2... 0/2 = 0
static const int RED_SATURATION    = 255;
static const int RED_VALUE         = 255;
static const int HSV_WINDOW        =  20;

static const int CURRENT_COLOR_RED   = 0;

static const int NO_BUTTON_FOUND = -1;

static const int LEFT = 0;
static const int RIGHT = 1;

static const int CURRENT_TASK_WALK_TO_BUTTON = 0;
static const int CURRENT_TASK_PRESS_BUTTON   = 1;
static const int CURRENT_TASK_WALK_TO_FINISH = 2;


string LEFT_FOOT_FRAME_NAME = "/leftFoot";
string RIGHT_FOOT_FRAME_NAME = "/rightFoot";

class Qual2
{
    // Misc ROS stuff we need
    ros::NodeHandle nh_;
    
    // Variables we'll need for walking
    ros::Publisher footstepList_pub;
    ros::Subscriber footstepStatus_sub;
    
    // Variables we'll need for receiving and processing point cloud data
    ros::Subscriber sub;
    tf::TransformListener listener;
    
    pcl::PointXYZRGB buttonLocation;
    
    ros::Publisher marker_pub;
    visualization_msgs::Marker marker;
    float markerPulseIndex;
    
    int currentTask;
    
    ihmc_msgs::FootstepDataListRosMessage footstepListMsg;
    bool sentFootsteps;
    
public:
    /**
     * Default constructor
     * This constructor initializes the variables, sets up the publisher and subscriber
     */
    Qual2()
    {
        
        // Subscribe to the camera's point cloud data
        sub = nh_.subscribe ("/multisense/image_points2_color", 1,
                             &Qual2::cloud_cb, this);
        
        // Publisher to control robot
        footstepList_pub = nh_.advertise<ihmc_msgs::FootstepDataListRosMessage>("/ihmc_ros/valkyrie/control/footstep_list", 1);
        footstepStatus_sub = nh_.subscribe("/ihmc_ros/valkyrie/output/footstep_status", 1, &Qual2::updateFootsteps_cb, this);
        
        // Publisher to help visualize our answer
        marker_pub = nh_.advertise<visualization_msgs::Marker>("visualization_marker", 1);

        // Set initial button location
        buttonLocation.x = 0;
        buttonLocation.y = 0;
        buttonLocation.z = -5000;        
        
        // Set the frame ID and timestamp.  See the TF tutorials for information on these.
        marker.header.frame_id = "/head";
        // Set the namespace and id for this marker.  This serves to create a unique ID
        // Any marker sent with the same namespace and id will overwrite the old one
        marker.ns = "Qual2";
        marker.id = 0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.lifetime = ros::Duration();
        markerPulseIndex = 0;
        
        currentTask = CURRENT_TASK_WALK_TO_BUTTON;
        //walkToButton();
        //hardcodedWalkToButton();
        sentFootsteps = false;
    }

    /**
     * The deconstructor
     */
    ~Qual2()
    {
    }
    
    void doStuff()
    {
        if(!sentFootsteps)
        {
            //hardcodedWalkToButton();
            walkToButton();
            
            ROS_INFO("Publishing footstep list");
            footstepList_pub.publish(footstepListMsg);
            sentFootsteps = true;
            ROS_INFO("Footstep list sent");
        }
    }
    
    /**
     * 
     */
    void hardcodedWalkToButton()
    {
        ROS_INFO("Creating hardcoded footstep list");
        
        footstepListMsg.transfer_time = 1.5;
        footstepListMsg.swing_time = 1.5;
        footstepListMsg.execution_mode = 0;
        footstepListMsg.unique_id = -1;

        ihmc_msgs::FootstepDataRosMessage footstep01;
        footstep01.origin = 0;
        footstep01.robot_side = 0;
        footstep01.location.x = 0.214943381641;
        footstep01.location.y = 0.0147519645633;
        footstep01.location.z = 0.0905780933645;
        footstep01.orientation.x = 0.0;
        footstep01.orientation.y = 0.0;
        footstep01.orientation.z = -0.00117783502625;
        footstep01.orientation.w = 0.999999306225;
        //predicted_contact_points: []
        footstep01.trajectory_type = 0;
        footstep01.swing_height = 0.0;
        footstep01.unique_id = 0;
        
        ihmc_msgs::FootstepDataRosMessage footstep02;
        footstep02.origin = 0;
        footstep02.robot_side = 1;
        footstep02.location.x = -0.164407460517;
        footstep02.location.y = 0.0147519645633;
        footstep02.location.z = 0.0905738042005;
        footstep02.orientation.x = 0.0;
        footstep02.orientation.y = 0.0;
        footstep02.orientation.z = 0.00141339684782;
        footstep02.orientation.w = 0.999999001018;
        footstep02.trajectory_type = 0;
        footstep02.swing_height = 0.0;
        footstep02.unique_id = 0;
        
        ihmc_msgs::FootstepDataRosMessage footstep03;
        footstep03.origin = 0;
        footstep03.robot_side = 0;
        footstep03.location.x = 0.614942271651;
        footstep03.location.y = 0.0138096971084;
        footstep03.location.z = 0.0905891504815;
        footstep03.orientation.x = 0.0;
        footstep03.orientation.y = 0.0;
        footstep03.orientation.z = -0.00117783502625;
        footstep03.orientation.w = 0.999999306225;
        footstep03.trajectory_type = 0;
        footstep03.swing_height = 0.0;
        footstep03.unique_id = 0;
        
        
        
        ROS_INFO("Adding footsteps to list");
        footstepListMsg.footstep_data_list.push_back(footstep01);
        footstepListMsg.footstep_data_list.push_back(footstep02);
        footstepListMsg.footstep_data_list.push_back(footstep03);
        
        
        /*
        for(int i=0; i<numSteps; ++i)
        {
            
            
            
            pt.point.x += stepOffset;
            if(useLeftFoot)
            {
                pt.header.frame_id = LEFT_FOOT_FRAME_NAME;
                //footstepListMsg.footstep_data_list.push(createFootStepOffset(LEFT, pt));
                footstepListMsg.footstep_data_list[i] = createFootStepOffset(LEFT, pt);
            }
            else
            {
                pt.header.frame_id = RIGHT_FOOT_FRAME_NAME;
                //footstepListMsg.footstep_data_list.push(createFootStepOffset(RIGHT, pt));
                footstepListMsg.footstep_data_list[i] = createFootStepOffset(RIGHT, pt);
            }
        }
        */
    }
    
    /**
     * 
     * 
     */
    void walkToButton()
    {
        ROS_INFO("Walking to button");
        
        //footstepListMsg = ihmc_msgs::FootstepDataListRosMessage()
        ihmc_msgs::FootstepDataListRosMessage footstepListMsg;
        
        footstepListMsg.transfer_time = 1.5;
        footstepListMsg.swing_time = 1.5;
        footstepListMsg.execution_mode = 0;
        footstepListMsg.unique_id = -1;
        
        geometry_msgs::PointStamped pt;
        pt.header.stamp = ros::Time(0);
        pt.point.x = 0.0f;
        pt.point.y = 0.0f;
        pt.point.z = 0.0f;
        //tf::Vector3f vec;
        //vec.x = 0.0f;
        //vec.y = 0.0f;
        //vec.z = 0.0f;
        int numSteps = 12;
        float stepOffset = 0.2;
        bool useLeftFoot = true;
        
        ROS_INFO("Planning my steps");
        for(int i=0; i<numSteps; ++i)
        {
            pt.point.x += stepOffset;
            if(useLeftFoot)
            {
                pt.header.frame_id = LEFT_FOOT_FRAME_NAME;
                //footstepListMsg.footstep_data_list.push(createFootStepOffset(LEFT, pt));
                footstepListMsg.footstep_data_list[i] = createFootStepOffset(LEFT, pt);
            }
            else
            {
                pt.header.frame_id = RIGHT_FOOT_FRAME_NAME;
                //footstepListMsg.footstep_data_list.push(createFootStepOffset(RIGHT, pt));
                footstepListMsg.footstep_data_list[i] = createFootStepOffset(RIGHT, pt);
            }
            
            ROS_INFO("Created step %d", i);
        }
        
/*        
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [0.2, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.4, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [0.6, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.8, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [1.0, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.2, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [1.4, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.6, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [1.8, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.0, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(LEFT, [2.2, 0.0, 0.0]));
        footstepListMsg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.4, 0.0, 0.0]));
  */      
        //footstepList_pub.publish(footstepListMsg);
    }
    
    // Creates footstep offset from the current foot position. The offset is in foot frame.
    ihmc_msgs::FootstepDataRosMessage createFootStepOffset(int stepSide, geometry_msgs::PointStamped offset)
    {
        ROS_INFO("Creating footstep offset");
        ihmc_msgs::FootstepDataRosMessage footstep;
        footstep = createFootStepInPlace(stepSide);
    
        // transform the offset to world frame
        /*
        tf::Quaternion quat;
        quat = footstep.orientation;
        rot = tf::transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w]);
        transformedOffset = numpy.dot(rot[0:3, 0:3], offset);
        */
        
        ////////////////////////
        
        //geometry_msgs::PointStamped pt;
        geometry_msgs::PointStamped pt_transformed;
        ////pt.header = myCloud->header;
        //pt.point.x = offset.point.x;
        //pt.point.y = offset.point.y;
        //pt.point.z = offset.point.z;
        
        //tf::TransformListener listener;
        //listener.transformPoint("world", pt, pt_transformed);
        try
        {
            listener.waitForTransform("/world", LEFT, ros::Time(0), ros::Duration(1.0));
            listener.transformPoint("/world", offset, pt_transformed);
        }
        catch (tf::TransformException ex){
            ROS_ERROR("[Qual2::createFootStepOffset()] %s",ex.what());
        }
    
        //////////////////
        /*
        tf::Transform transform;
        transform.setOrigin( offset );
        tf::Quaternion q;
        q.setRPY(0, 0, 0); // Walk straight
        transform.setRotation(q);
        //br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "turtle1", "carrot1"));
        */
        ///////////////////
        
        //footstep.location.x += transformedOffset[0];
        //footstep.location.y += transformedOffset[1];
        //footstep.location.z += transformedOffset[2];

        footstep.location.x = pt_transformed.point.x;
        footstep.location.y = pt_transformed.point.y;
        footstep.location.z = pt_transformed.point.z;
        
        ROS_INFO("Footstep offset created");
        return footstep;
    }
    
    /**
     * Creates footstep with the current position and orientation of the foot.
     * 
     * I think this is supposed to assume 0,0,0 and then the transform to /world gives the location??
     * 
     */
    ihmc_msgs::FootstepDataRosMessage createFootStepInPlace(int stepSide)
    {
        ROS_INFO("Where's my foot?");
        ihmc_msgs::FootstepDataRosMessage footstep;
        
        footstep.robot_side = stepSide; 
        
        string foot_frame;
        if (stepSide == LEFT)
        {
            foot_frame = LEFT_FOOT_FRAME_NAME;
        }
        else
        {
            foot_frame = RIGHT_FOOT_FRAME_NAME;
        }
        
        tf::StampedTransform footWorldTransform;
        //geometry_msgs::PointStamped pt;
        //geometry_msgs::PointStamped pt_transformed;
        //pt.header.stamp = ros::Time(0);
        //pt.header.frame_id = foot_frame;
        //pt.point.x = 0;
        //pt.point.y = 0;
        //pt.point.z = 0;
        
        try{
            listener.waitForTransform("/world", foot_frame, ros::Time(0), ros::Duration(1.0));
            listener.lookupTransform("/world", foot_frame, ros::Time(0), footWorldTransform);
            
//            listener.transformPoint("/world", pt, pt_transformed);
        }
        catch (tf::TransformException ex){
            ROS_ERROR("[Qual2::createFootStepInPlace()] %s",ex.what());
        }
        
        footstep.orientation.x = (float)(footWorldTransform.getRotation().x);
        footstep.orientation.y = footWorldTransform.getRotation().y;
        footstep.orientation.z = footWorldTransform.getRotation().z;
        footstep.orientation.w = footWorldTransform.getRotation().w;
        footstep.location.x = footWorldTransform.getOrigin()[0];
        footstep.location.y = footWorldTransform.getOrigin()[1];
        footstep.location.z = footWorldTransform.getOrigin()[2];
        //footstep.location.x = pt_transformed.point.x;
        //footstep.location.y = pt_transformed.point.y;
        //footstep.location.z = pt_transformed.point.z;
        
        ROS_INFO("Ah, there it is!");
        return footstep;
    }
    
    /**
     * Creates footstep with the current position and orientation of the foot.
     * 
     * I think this is supposed to assume 0,0,0 and then the transform to /world gives the location??
     * 
    ihmc_msgs::FootstepDataRosMessage createFootStepInPlace_TEMP(int stepSide)
    {
        ROS_INFO("Where's my foot?");
        ihmc_msgs::FootstepDataRosMessage footstep;
        
        footstep.robot_side = stepSide; 
    
        string foot_frame;
        if (stepSide == LEFT)
        {
            foot_frame = LEFT_FOOT_FRAME_NAME;
        }
        else
        {
            foot_frame = RIGHT_FOOT_FRAME_NAME;
        }
        
        tf::StampedTransform footWorldTransform;
        geometry_msgs::PointStamped pt;
        geometry_msgs::PointStamped pt_transformed;
        pt.header.stamp = ros::Time(0);
        pt.header.frame_id = foot_frame;
        pt.point.x = 0;
        pt.point.y = 0;
        pt.point.z = 0;
        
        try{
            listener.waitForTransform("/world", foot_frame, ros::Time(0), ros::Duration(1.0));
            listener.lookupTransform("/world", foot_frame, ros::Time(0), footWorldTransform);
            
            listener.transformPoint("/world", pt, pt_transformed);
        }
        catch (tf::TransformException ex){
            ROS_ERROR("[Qual2::createFootStepInPlace()] %s",ex.what());
        }
            
        footstep.orientation = footWorldTransform.getRotation();
        footstep.location.x = footWorldTransform.translation[0];
        //footstep.location.x = pt_transformed.point.x;
        //footstep.location.y = pt_transformed.point.y;
        //footstep.location.z = pt_transformed.point.z;
        
        ROS_INFO("Ah, there it is!");
        return footstep;
    }
    */
    
    /**
     * The callback function for when we receive pointcloud data from the robot
     */
    void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
    {
        
        publishMarker();
    }
    
    void updateFootsteps_cb (const ihmc_msgs::FootstepStatusRosMessage input)
    {
        ROS_INFO_ONCE("Receiving footstep status");
    }
    
    /**
     * 
     */
    bool hasSubscribers()
    {
        return (footstepList_pub.getNumSubscribers() > 0);
    }
    
    void publishMarker()
    {
        marker.header.stamp = ros::Time::now();
        
        // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
        if(buttonLocation.x != buttonLocation.x)
        { // Check for NAN
            marker.pose.position.x = 0;
            marker.pose.position.y = 0;
            marker.pose.position.z = -5000;
        }
        else
        {
            marker.pose.position.x = buttonLocation.x;
            marker.pose.position.y = buttonLocation.y;
            marker.pose.position.z = buttonLocation.z;
            
            // Set the scale of the marker -- 1x1x1 here means 1m on a side
            markerPulseIndex += 0.2;
            marker.scale.x = 0.01+(sin(markerPulseIndex)/10);
            marker.scale.y = 0.01+(sin(markerPulseIndex)/10);
            marker.scale.z = 0.01+(sin(markerPulseIndex)/10);
        }
        
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        
        // Set the color -- be sure to set alpha to something non-zero!
        marker.color.r = 1.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;
        
        marker_pub.publish(marker);
    }

}; // End of Qual2 class


/**
 * The main function - This is where the program starts
 */
int main(int argc, char** argv)
{
    // Initialize ROS stuff
    ros::init(argc, argv, "Qual2");

    // Create our Qual2 object
    Qual2 myQual2;

    // Keep looping until we're done
    while( ros::ok() )
    {
        if(myQual2.hasSubscribers())
        {
            myQual2.doStuff();
        }
        else
        {
            ROS_INFO_ONCE("Waiting for subscribers...");
        }
        
        // Allow other ROS nodes to do their thing
        ros::spinOnce();
    }
    return 0;
}
