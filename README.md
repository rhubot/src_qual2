# README #
Space Robotics Challenge  
Team Rhubarb  
Qualification Task 2  

### Setup ###
* [Create a catkin workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)
* Get the source code  
```$ git clone https://rhubot@bitbucket.org/rhubot/src_qual2.git```

### Compile ###

```
$ cd ~/catkin_ws/
$ catkin_make
```

### Run ###
```
$ source ~/catkin_ws/devel/setup.bash
$ roslaunch srcsim qual2.launch extra_gazebo_args:="-r" init:="true"
$ roscd src_qual2/src/
$ python myQual2.py
```